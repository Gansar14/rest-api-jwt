<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PinjambukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_pinjam' =>['required'],
            'tanggal_bataspinjam' => ['required'],
            'tanggal_pengembalian' =>['required'],
            'status'=> ['required'],
            'buku'=>['required'],
        ]);

       $pinjambuku= auth()->user()->pinjambuku()->create([
            'tanggal_pinjam'=>request('tanggal_pinjam'),
            'tanggal_bataspinjam'=>request('tanggal_bataspinjam'),
            'tanggal_pengembalian'=>request('tanggal_pengembalian'),
            'status'=>request('status'),
            'buku_id'=>request('buku'),
        ]);
        return $pinjambuku;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjambukus $pinjambuku)
    {
        // return $pinjambuku;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
