<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nim'=>['required','min:3','max:10'],
            'fakultas'=>['required'],
            'jurusan'=>['required'],
            'nohp'=>['required'],
            'nowa'=>['required'],
          
            ]);

           $profiles= auth()->user()->profiles()->create([
                'nim' => request('nim'),
                'fakultas' => request('fakultas'),
                'jurusan' => request('jurusan'),
                'nohp' => request('nohp'),
                'nowa' => request('nowa'),
           
            ]);
                return $profiles;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
