<?php

namespace App\Models\Perpus;

use Illuminate\Database\Eloquent\Model;

class Pinjambuku extends Model
{
    protected $fillable = ['tanggal_pinjam','tanggal_bataspinjam','tanggal_pengembalian','status','buku_id'];

    // //
    // public function getRouteKeyName()
    // {
    //     return tanggal_pinjam;
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    
    public function buku()
    {
        return $this->belongsTo(Buku::class);
    }

}

