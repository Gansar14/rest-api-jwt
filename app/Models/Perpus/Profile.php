<?php

namespace App\Models\Perpus;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['nim','fakultas','jurusan','nohp','nowa'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    // public function profile()
    // {
    //     return $this->belongsTo()
    // }

}
