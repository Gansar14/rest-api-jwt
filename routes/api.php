<?php

Route::namespace('Auth')->group(function(){
    Route::post('register','RegisterController');
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
    
});


Route::namespace('Perpus')->middleware('auth:api')->group(function(){
    Route::post('profile','ProfileController@store');
    Route::post('pinjam-buku','PinjambukuController@store');
});

// Route::get('pinjambukus/{pinjambuku}','Perpus\PinjambukuController@show');
Route::get('user','UserController');